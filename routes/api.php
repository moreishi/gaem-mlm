<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {

    Route::prefix('codes')->group(function() {
        Route::post('generate','CodeController@generate');
        Route::post('transfer','CodeController@transfer');
    });

    Route::prefix('wallet')->group(function() {
        Route::post('withdraw','WalletController@withdraw');
        Route::post('withdraw-complete','WalletController@withdraw_process_complete');
    });

    Route::prefix('profile')->group(function() {
        Route::get('me','ProfileController@me');
        Route::post('me','ProfileController@update_me');
    });

    Route::prefix('accounts')->group(function() {
        Route::put('change_my_password','AccountController@change_my_password');
        Route::put('update_avatar','AccountController@updateAvatar');
        Route::post('activate','AccountController@activate');
    });

    Route::prefix('gifts')->group(function() {
        Route::post('transfer','GiftController@transfer');
    });

    Route::prefix('orders')->group(function() {
        Route::post('transfer','OrderController@transfer');
        Route::post('activate','OrderController@activate');
    });

    Route::prefix('documents')->group(function() {
        Route::post('upload','DocumentController@upload');
        Route::get('approve/{id}','DocumentController@approve');
        Route::get('deny/{id}','DocumentController@deny');
    });

    Route::prefix('accounts')->group(function() {
        Route::post('upgrade/{accountId}', 'Account\AccountUpgradeController@upgrade')->name('account-upgrade');
    });

    // resource
    Route::resource('document', 'DocumentController');
    Route::resource('packages', 'PackageController');
    Route::resource('gifts', 'GiftController');
    Route::resource('accounts', 'AccountController');
    Route::resource('profile', 'ProfileController');
    Route::resource('codes', 'CodeController');
    Route::resource('wallet', 'WalletController');
    Route::resource('genealogy', 'GenealogyController');
    Route::resource('orders', 'OrderController');

});

