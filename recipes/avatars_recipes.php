<?php

namespace Deployer;

desc('Copying avatar from production');
task('avatar:production', function() {
    run("cp /var/www/gaem/dashboard/public_html/public/media/avatars/* {{release_path}}/public/media/avatars/");
});