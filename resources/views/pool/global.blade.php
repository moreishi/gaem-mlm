@extends('layouts.backend')

@section('content')

    <header-common page-title="Global Pool Qualifiers" main-menu="Global Pool" sub-menu="Qualifiers"></header-common>

    <div class="col-xl-12">
        <div class="block">
            <div class="block-content">
                <table class="table table-vcenter">
                    <thead class="thead-light">
                    <tr>
                        <th class="d-none d-sm-table-cell">User</th>
                        <th class="d-none d-sm-table-cell text-center">Direct Invite</th>
                        <th class="d-none d-sm-table-cell text-center">Matched Points</th>
                        {{--<th class="text-center">Activated At</th>--}}
                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td colspan="4" class="text-center">No records found</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection