@extends('layouts.backend')

@section('content')
    <header-common page-title="Wallet Transactions" main-menu="Wallet" sub-menu="Transactions"></header-common>


    <div class="col-xl-12">
        <div class="block">
            <div class="block-content">
                <wallet-transaction transactions="{{ json_encode($transactions->items()) }}"></wallet-transaction>
                {{ $transactions->links() }}
            </div>
        </div>
    </div>


@endsection