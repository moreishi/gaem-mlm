@extends('layouts.backend')

@section('content')

    <header-common page-title="Wallet Withdraw" main-menu="Wallet" sub-menu="Withdraw"></header-common>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <Block block-title="WITHDRAWABLE AMOUNT">
                    <h1>{{config('currency.default')}}{{number_format($cashout, 2, '.', ',')}}</h1>
                </Block>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p><strong>Note:</strong> Our daily cut-off payout request is until 12PM only. Minimum withdrawal amount is {{ config('currency.default') }}{{ config('withdraw.minimum.wallet') }}</p>
                @if($form_open)
                <wallet-withdraw tax="{{$tax}}" minimum="{{$minimum}}" fee="{{$fee}}"><wallet-withdraw>
                @endif

                {{--<a href="{{ route('documents.verification') }}" class="btn btn-primary">Verify Account</a>--}}

            </div>
        </div>

    </div>

@endsection