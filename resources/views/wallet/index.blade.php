@extends('layouts.backend')

@section('content')

    <header-common page-title="Wallet Summary" main-menu="Wallet" sub-menu="Summary"></header-common>
    <div class="content">
        <div class="row">
            <div class="col-md-6">
                <Block block-title="WITHDRAWABLE AMOUNT">
                    <h1>{{config('currency.default')}}{{number_format($summary['cashout'], 2, '.', ',')}}</h1>
                </Block>
            </div>
            <div class="col-md-6">
                <Block block-title="TRAVEL FUNDS">
                    <h1>{{config('currency.default')}}{{number_format($summary['travel'], 2, '.', ',')}}</h1>
                </Block>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <Block block-title="REFERRAL INCOME" bgcolor="bg-success" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['invites'], 2, '.', ',')}}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="PAIRING INCOME" bgcolor="bg-info" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['pairing'], 2, '.', ',')}}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="ROYALTY INCOME" bgcolor="bg-warning" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['royalty'], 2, '.', ',')}}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="UNILEVEL INCOME" bgcolor="bg-primary-darker" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['unilevel'], 2, '.', ',')}}</h1>
                </Block>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-xl-4">
                <a href="/wallet/withdraw" class="btn btn-primary">Withdraw</a>
                <a href="/wallet/transactions" class="btn btn-primary">Transactions</a>
            </div>
        </div>
    </div>

@endsection