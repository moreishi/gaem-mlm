@extends('layouts.backend')

@section('content')
    <header-common page-title="Code Transfer" main-menu="Code" sub-menu="Transfer"></header-common>
    <div class="content"><code-transfer></code-transfer></div>
@endsection