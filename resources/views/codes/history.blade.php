@extends('layouts.backend')

@section('content')
    <header-common page-title="Code History" main-menu="Code" sub-menu="History"></header-common>

    <div class="col-xl-12">
        <div class="block">
            <div class="block-content">
                <code-history transfers="{{ json_encode($transfers->items()) }}" user-id="{{Auth::user()->id}}"></code-history>
                {{ $transfers->links() }}
            </div>
        </div>
    </div>

@endsection