@extends('layouts.backend')

@section('content')
    <header-common page-title="Codes Available" main-menu="Codes" sub-menu="Available"></header-common>
    {{--<Codes codes="{{$codes}}" />--}}
    <div class="container">
        <div class="row mt-4">
            <div class="col-md-6">
                <Block block-title="STARTER PACKAGE">
                    <h1>{{$starter}}</h1>
                </Block>
            </div>
            <div class="col-md-6">
                <Block block-title="BUILDER PACKAGE">
                    <h1>{{$builder}}</h1>
                </Block>
            </div>
            <div class="col-md-6">
                <Block block-title="ADVANCE PACKAGE">
                    <h1>{{$advance}}</h1>
                </Block>
            </div>
        </div>
    </div>
@endsection