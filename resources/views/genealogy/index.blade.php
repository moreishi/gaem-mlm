@extends('layouts.backend')

@section('content')
    <header-common page-title="Genealogy" main-menu="Genealogy"></header-common>
    <div style="overflow: auto;overflow-y: hidden;">
        <genealogy accounts-data="{{ $accounts }}" nodes-data="{{ $nodes }}" codes-data="{{ $codes }}"></genealogy>
    </div>
@endsection