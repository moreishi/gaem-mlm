@extends('layouts.backend')

@section('content')
    <head-bg page-title="Dashboard" bgcolor="bg-danger">
        Welcome back, {{Auth::user()->firstname}}!
    </head-bg>

    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <Block block-title="REFERRAL INCOME" bgcolor="bg-success" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['invites'], 2, '.', ',')}}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="PAIRING INCOME" bgcolor="bg-info" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{ number_format($summary['pairing'], 2, '.', ',') }}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="ROYALTY INCOME" bgcolor="bg-warning" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['royalty'], 2, '.', ',')}}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="UNILEVEL INCOME" bgcolor="bg-primary-darker" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['unilevel'], 2, '.', ',')}}</h1>
                </Block>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">Total Commission</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 style="">{{config('currency.default')}} {{number_format($summary['total_commission'], 2, '.', ',')}}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">WITHDRAWABLE AMOUNT</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 style="">{{config('currency.default')}}{{number_format($summary['cashout'], 2, '.', ',')}}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <Block block-title="FOLLOWERS">
                    <h1>{{$summary['followers']}}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="AVAILABLE CODES">
                    <h1>{{$summary['codes']}}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="PAYOUT">
                    <h1>{{config('currency.default')}}{{number_format($summary['payouts'], 2, '.', ',')}}</h1>
                </Block>
            </div>
            <div class="col-md-3">
                <Block block-title="TRAVEL FUND">
                    <h1>{{config('currency.default')}}{{number_format($summary['travel'], 2, '.', ',')}}</h1>
                </Block>
            </div>
        </div>

        <!-- start -->
        <div class="row">
            <div class="col-md-6">
                <Block block-title="GLOBAL POOL"  bgcolor="bg-success" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['global_pool'], 2, '.', ',')}}</h1>
                </Block>
            </div>
            <div class="col-md-6">
                <Block block-title="WEEKLY LEADERSHIP FUND"  bgcolor="bg-danger" textcolor="color:#ffffff;">
                    <h1 style="color:#ffffff;">{{config('currency.default')}}{{number_format($summary['leadership_fund'], 2, '.', ',')}}</h1>
                </Block>
            </div>
        </div>
        <!-- end -->

        <!-- New Members -->
        <div class="row row-deck">
            <div class="col-xl-6">
                <div class="col-xl-12" style="padding: 0;">
                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">Last 10 Signed Up Members</h3>
                        </div>
                        <div class="block-content">
                            <ul class="nav-items push">
                                @foreach($members as $member)
                                <li>
                                    <a class="media py-3" href="javascript:void(0)">
                                        <div class="mr-3 ml-2 overlay-container overlay-bottom">
                                            <img class="img-avatar img-avatar48" src="{{ $member->avatar }}" alt="">
                                            {{--<span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>--}}
                                        </div>
                                        <div class="media-body">
                                            <div class="font-w600">{{ $member->firstname }} {{ $member->lastname }}</div>
                                            <div class="font-size-sm text-muted">{{ $member->city }}</div>
                                        </div>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 2nd column -->
            <div class="col-xl-6">



                <div class="col-xl-12" style="padding: 0;">
                    @if($sponsor)
                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">YOUR DIRECT SPONSOR</h3>
                        </div>

                        <div class="block-content" style="padding-top:0;">
                            <a href="javascript:void(0)" class="media py-3">
                                <div class="mr-3 ml-2 overlay-container overlay-bottom">
                                    <img src="{{$sponsor->avatar}}" alt="" class="img-avatar img-avatar96">
                                </div>
                                <div class="media-body">
                                    <div class="font-w600">{{$sponsor->firstname}}{{$sponsor->lastname}}</div>
                                    <div class="font-size-sm text-muted">Member since {{ \Carbon\Carbon::parse($sponsor->created_at)->format('F jS, Y') }}</div>
                                </div>
                            </a>
                        </div>

                    </div>
                    @endif

                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">Weekly Leadership Qualifiers</h3>
                        </div>
                        <div class="block-content">
                            <ul class="nav-items push">
                                @foreach($leadership_qualifiers as $member)
                                    <li>
                                        <a class="media py-3" href="javascript:void(0)">
                                            <div class="mr-3 ml-2 overlay-container overlay-bottom">
                                                <img class="img-avatar img-avatar48" src="{{ $member->avatar }}" alt="">
                                                {{--<span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>--}}
                                            </div>
                                            <div class="media-body">
                                                <div class="font-w600">Matched Points: {{$member->firstname }}</div>
                                                <div class="font-size-sm text-muted">Weekly Points: {{$member->walletPairing->count() }} | Weekly Invites: {{$member->childrenWeek->count() }}</div>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Global Pool -->
        </div>
        <!-- End Members -->

    </div>
@endsection
