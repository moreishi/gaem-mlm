@extends('layouts.backend')

@section('content')

    <header-common page-title="Products" main-menu="Products"></header-common>

    <div class="container">
        <div class="row">

            <div class="col-md-12 mt-3">
                <form action="be_blocks_forms.html" method="POST">
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Create Product</h3>
                            <div class="block-options">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-sm btn-secondary">
                                    Reset
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-sm-3 py-md-5">
                                <div class="col-sm-10 col-md-8">
                                    <div class="form-group">
                                        <label for="block-form1-username">Product Name</label>
                                        <input type="text" class="form-control form-control-alt" id="block-form1-username" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="block-form1-username">Description</label>
                                        <input type="text" class="form-control form-control-alt" id="block-form1-username" name="description">
                                    </div>
                                    <div class="form-group">
                                        <label for="block-form1-username">SKU</label>
                                        <input type="text" class="form-control form-control-alt" id="block-form1-username" name="sku">
                                    </div>
                                    <div class="form-group">
                                        <label for="block-form1-username">Price</label>
                                        <input type="text" class="form-control form-control-alt" id="block-form1-username" name="price">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection