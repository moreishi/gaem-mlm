@extends('layouts.backend')

@section('content')
    <header-common page-title="Edit User" main-menu="User" sub-menu="Edit"></header-common>

    <div class="container mb-7">

        <div class="row">
            <div class="col-md-12 mt-5">
                <a href="/users" class="btn btn-primary">View All Users</a>
            </div>
        </div>

        <div class="mt-5">
            <user-edit user-data="{{ json_encode($user) }}" :user-id={{(int)$user->id}}></user-edit>
        </div>
    </div>


@endsection
