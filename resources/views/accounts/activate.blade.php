@extends('layouts.backend')

@section('content')
    <header-common page-title="Accounts" main-menu="Accounts" sub-menu="Activate"></header-common>
    <div class="content">
        <Account-Activate codes-data="{{ $codes }}"></Account-Activate>

    </div>
@endsection