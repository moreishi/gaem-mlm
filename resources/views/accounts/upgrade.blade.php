@extends('layouts.backend')

@section('content')
    <header-common page-title="Accounts" main-menu="Accounts" sub-menu="Upgrade"></header-common>
    <div class="content">

        <div class="col-md-12">
            <account-upgrade
                    business="{{ $business }}"
                    advance="{{ $advance }}"
                    account-id="{{ $account->id }}"></account-upgrade>
        </div>

    </div>
@endsection