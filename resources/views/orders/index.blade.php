@extends('layouts.backend')

@section('content')
    <header-common page-title="Orders" main-menu="Orders"></header-common>
    <Orders orders-data="{{$orders}}"></Orders>

    @if(Auth::user()->hasAnyRole(['admin','member']))
    <div class="row items-push-2x text-center text-sm-left">
        <div class="col-sm-6 col-xl-4 ml-3">
            <a href="/orders/new" class="btn btn-primary">New Order</a>
        </div>
    </div>
    @endif
@endsection