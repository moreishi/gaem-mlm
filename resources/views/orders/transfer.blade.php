@extends('layouts.backend')

@section('content')
    <header-common page-title="Transfer Order" main-menu="Transfer" sub-menu="Order"></header-common>
    <transfer-order orders-data="{{ $orders }}" ></transfer-order>
@endsection