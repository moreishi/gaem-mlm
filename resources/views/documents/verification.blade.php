@extends('layouts.backend')

@section('content')
    <header-common page-title="Account Verification" main-menu="Account" sub-menu="Verification"></header-common>
    <div class="content">
        <div class="row">

            <div class="col-md-12">

                <div class="block">
                    <ul class="nav nav-tabs nav-tabs-block" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="step1-tab" data-toggle="tab" href="#step1" role="tab" aria-controls="account" aria-selected="true">STEP 1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="step2-tab" data-toggle="tab" href="#step2" role="tab" aria-controls="profile" aria-selected="false">STEP 2</a>
                        </li>
                    </ul>
                    <div class="block-content tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="step1" role="tabpanel" aria-labelledby="step1-tab">

                            {{-- start --}}
                            <form action="be_blocks_forms.html" method="POST">
                                <div class="block">

                                    <div class="block-header block-header-default">
                                        <h3 class="block-title">UPLOAD PRIMARY IDENTIFICATION</h3>
                                        <div class="block-options">
                                        </div>
                                    </div>
                                    <div class="block-content">

                                        <p>We need to verify your account, upload and submit your primary identification card.</p>

                                        <div class="row justify-content-center py-sm-3 py-md-5">

                                            <div class="col-sm-10 col-md-8">
                                                @if(!$frontId)
                                                <document-upload description="FRONT OF ID" label="FRONT"></document-upload>
                                                @else
                                                <p style="color:green" class="font-size-sm">File Image FRONT OF ID has been uploaded and pending for review.</p>
                                                @endif
                                                @if(!$backId)
                                                <document-upload description="BACK OF ID" label="BACK"></document-upload>
                                                @else
                                                <p style="color:green" class="font-size-sm">File Image BACK OF ID has been uploaded and pending for review.</p>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                            {{-- end --}}


                        </div>
                        <div class="tab-pane fade" id="step2" role="tabpanel" aria-labelledby="step2-tab">

                            <form action="be_blocks_forms.html" method="POST">
                                <div class="block">
                                    <div class="block-header block-header-default">
                                        <h3 class="block-title">UPLOAD SELF PHOTO WITH ID</h3>
                                        <div class="block-options">
                                        </div>
                                    </div>
                                    <div class="block-content">

                                        {{--<p>Take a selfie photo holding your primary identification card and submit it to us.</p>--}}

                                        @if(!$selfieId)
                                        <div class="col-md-3 offset-md-4 mt-3">
                                            <img class="" src="/selfie.png" width="250px">
                                        </div>
                                        @endif
                                        
                                        <div class="row justify-content-center py-sm-3 py-md-5">
                                            <div class="col-sm-10 col-md-8">
                                                @if(!$selfieId)
                                                <document-upload label="SELFIE" description="SELFIE WITH ID"></document-upload>
                                                @else
                                                    <p v-if="!showInput" style="color:green" class="font-size-sm">File Image SELFIE WITH ID has been uploaded and pending for review.</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection