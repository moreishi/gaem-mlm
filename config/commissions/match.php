<?php
/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-03-02
 * Time: 15:07
 */
return [
    'SP' => [
        'match' => ['max' => 10],
        'value' => 100
    ],
    'BP' => [
        'match' => ['max' => 30],
        'value' => 100
    ],
    'AP' => [
        'match' => ['max' => 100],
        'value' => 100
    ],
];