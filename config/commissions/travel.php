<?php
/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-03-03
 * Time: 07:53
 */
return [
  'SP' => [
      'max_post' => 2,
      'amount' => 100,
      'currency' => 'PHP'
  ],
  'BP' => [
      'max_post' => 6,
      'amount' => 100,
      'currency' => 'PHP'
  ]
];