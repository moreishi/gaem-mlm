<?php

return [
    'SP' => [
        'depth' => 3,
        'cash' => 0.00,
        'gc' => [
            'value' => 100.00,
            'quantity' => 2
        ]
    ],
    'BP' => [
        'depth' => 3,
        'cash' => 150.00,
        'gc' => [
            'value' => 100.00,
            'quantity' => 3
        ]
    ],
    'AP' => [
        'depth' => 3,
        'cash' => 0.00,
        'gc' => [
            'value' => 0.00,
            'quantity' => 0
        ]
    ]
];