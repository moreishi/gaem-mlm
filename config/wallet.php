<?php
/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-02-05
 * Time: 17:50
 */

return [
    'withdraw' => [
        'fees' => [
            'admin' => 0,
            'processing' => 0
        ],
        'min' => 100
    ],
];