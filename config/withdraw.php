<?php
/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-03-06
 * Time: 18:03
 */
return [
    'fees' => [
        'processing' => 50.00,
        'tax' => [
            'percent' => 10,
            'value' => 10
        ]
    ],
    'minimum' => [
        'pairing' => 500,
        'royalty' => 1000,
        'referral' => 1000,
        'unilevel' => 500,
        'wallet' => 1500
    ]
];