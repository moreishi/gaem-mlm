<?php

/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-02-19
 * Time: 23:59
 */

return [
    'SP' => [
        'id' => 1,
        'points' => 1,
        'name' => 'Starter Package',
        'value' => 598.00,
        'addOn' => [
            'gc' => [
                'value' => 100.00,
                'quantity' => 2
            ]
        ]

    ],
    'BP' => [
        'id' => 2,
        'points' => 3,
        'name' => 'Builder Package',
        'value' => 1698.00,
        'addOn' => [
            'gc' => [
                'value' => 100.00,
                'quantity' => 3
            ]
        ]
    ],
    'AP' => [
        'id' => 3,
        'points' => 10,
        'name' => 'Advance Package',
        'value' => 6398.00,
        'addOn' => [
            'gc' => [
                'value' => 100.00,
                'quantity' => 12
            ]
        ]
    ]
];