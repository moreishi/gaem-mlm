<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\AccountUpgradeEvent' => [
            'App\Listeners\AccountUpgrade\AccountAddGiftCertListener',
            'App\Listeners\AccountUpgrade\AccountUpdateDescendantBonusListener',
            'App\Listeners\AccountUpgrade\AccountUpgradeListener',
            'App\Listeners\AccountUpgrade\AccountUpdateCodeListener',
            'App\Listeners\AccountUpgrade\AddLeadershipFundListener',
            'App\Listeners\AccountUpgrade\DispatchUplineBonus',
        ],
        'App\Events\ActivateAccount' => [
            'App\Listeners\ActivateAccount\UpdateRole',
            'App\Listeners\ActivateAccount\PackageKit',
            'App\Listeners\ActivateAccount\InviteBonus',
        ],
        'App\Events\ActivateNode' => [
            'App\Listeners\ActivateNode\AddNode',
            'App\Listeners\ActivateNode\AddGiftCert',
            'App\Listeners\ActivateNode\UpdateCode',
            'App\Listeners\ActivateNode\UpdateDescendants',
            'App\Listeners\ActivateNode\AddLeadershipFund',
        ],
        'App\Events\RoyaltyCommission' => [
            'App\Listeners\RoyaltyCommission\AddRoyaltyCommission',
        ],
        'App\Events\ReferralCommission' => [
            'App\Listeners\ReferralCommission\AddReferralCommission',
        ],
        'App\Events\MatchCommission' => [
            'App\Listeners\MatchCommission\AddMatchCommission',
        ],
        'App\Events\TravelFund' => [
            'App\Listeners\TravelFund\AddTravelFund',
        ],
        'App\Events\Orders\TransferOrder' => [
            'App\Listeners\Orders\TransferOrder\TransferOrderCode',
        ],
        'App\Events\Orders\ActivateOrder' => [
            'App\Listeners\Orders\ActivateOrder\UpdatePointsValue',
        ],
        'App\Events\Jobs\CrawlGenealogyEvent' => [
            'App\Listeners\Jobs\CrawlGenealogyListener',
        ]

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
