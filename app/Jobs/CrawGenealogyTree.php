<?php

namespace App\Jobs;

use App\Services\Genealogy\GenealogyService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Node;
use App\User;
use Log;

class CrawGenealogyTree implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param Node $node
     */
    public function handle(Node $node)
    {
        $genealogy = new GenealogyService($node);
        $genealogy->store();

        Log::error("get_last_processed_node",[$genealogy->get_last_processed_node()]);
        Log::error("get_process_node_level",[$genealogy->get_process_node_level()]);
    }
}
