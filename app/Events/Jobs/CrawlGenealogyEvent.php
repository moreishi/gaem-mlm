<?php

namespace App\Events\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Node;

class CrawlGenealogyEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $node;

    /**
     * CrawlGenealogyEvent constructor.
     * @param Node $node
     */
    public function __construct(Node $node)
    {
        $this->node = $node;
    }

}