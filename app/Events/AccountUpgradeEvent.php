<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

use App\Node;
use App\User;
use App\Package;


class AccountUpgradeEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    public $node;

    public $package;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Node $node, Package $package)
    {

        $this->node = $node;

        $this->user = $user;

        $this->package = $package;

    }

}
