<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\User;
use App\Node;

class ReferralCommission
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    public $node;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Node $node)
    {
        $this->user = $user;

        $this->node = $node;
    }

}
