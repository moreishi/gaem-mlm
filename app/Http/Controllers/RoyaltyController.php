<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Wallet;
use Auth;

class RoyaltyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $royalty = $this->get_sum('royalty');
        return view('royalty.index',[
            'royalty' => $royalty,
            'button_open' => $this->isTodayEndOfWeek()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_wallet($source = 'royalty') {
        return json_encode(Wallet::where('source',$source)->get());
    }

    public function move() {

        $total = $this->get_sum('royalty');
        $minimum = $this->get_minimum();



        if($total >= $minimum && $this->isTodayEndOfWeek()) {
            Wallet::where(['user_id' => Auth::user()->id,'source' => 'royalty'])->update([
                'source' => 'cashout',
                'status' => 'PROCESSING'
            ]);
        }

        return redirect()->route('royalty');
    }

    public function get_minimum() {
        return (int) config('withdraw.minimum.royalty');
    }


    public function get_sum($source) {
        return Wallet::where([
            'user_id' => Auth::user()->id,'source' => $source])->sum('amount');
    }

    public function isTodayEndOfWeek() {
        if(Carbon::today()->format('Y-m-d') == Carbon::today()->endOfWeek()->format('Y-m-d'))
            return true;
        return false;
    }


}
