<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\Orders\TransferOrder;
use App\Events\Orders\ActivateOrder;

use App\Order;
use App\Product;
use App\User;
use App\Gift;
use Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasAnyRole(['admin','manager'])) {
            $orders = Order::orderBy('id','DESC')->get();
        } else {
            $orders = Order::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
        }

        return view('orders.index',['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = json_encode(Product::all());
        return view('orders.create',['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = User::where('username',$request->sold)->first();

        $products = json_encode([
            'scalar' => $request->products['scalar'],
            'mega_rub' => $request->products['mega_rub'],
            'coffee' => $request->products['coffee'],
            'gaem_fresh' => $request->products['gaem_fresh'],
            'perfume' => $request->products['perfume'],
            'longveggiety' => $request->products['longveggiety'],
//            'author' => ['user_id' => Auth::user()->id,'username' => Auth::user()->username]
        ]);

        Order::create([
            'data' => $products,
            'subject' => $request->subject,
            'note' => $request->note,
            'sold' => $request->sold,
            'user_id' => Auth::user()->id,
        ]);

        return response()->json(['request' => $request->all()]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function transferView()
    {
        $orders = Order::where(['user_id' => Auth::user()->id])->get();

        return view('orders.transfer',[
            'orders' => json_encode($orders)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function transfer(Request $request)
    {
        $request->validate([
            'username' => 'required|exists:users,username',
            'code' => 'required|exists:orders,code',
        ]);


        $order = Order::where(['code' => $request->code,'user_id' => Auth::user()->id]);
        if($order->count() < 1) return response()->json(['message' => 'Unable to complete process'],422);

        $user = User::where('username',$request->username)->first();
        $order = $order->first();

        event(new TransferOrder($order,$user));

        return response()->json(['message' => 'Your order is being processed']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activateView()
    {

//        if(Auth::user()->hasAnyRole(['admin','member'])) {
//            $order = json_encode(Order::all());
//        } else {
            $order = json_encode(Order::where('user_id',Auth::user()->id)->get());
//            $discounts = json_encode(Gift::where('user_id',Auth::user()->id)->get());
//        }

        return view('orders.activate',[
            'orders' => $order
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        $request->validate([
            'order_code' => 'required|exists:orders,code'
        ]);

        $order = Order::where(['user_id' => Auth::user()->id,'code' => $request->order_code]);

        if($order->count() < 1)
            return response()->json(['message' => 'Invalid order code'],422);

        $user = User::find(Auth::user()->id);
        $order = $order->first();

        event(new ActivateOrder($user,$order));

        return response()->json(['message' => 'Your order is being processed']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
