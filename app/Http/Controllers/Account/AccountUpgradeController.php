<?php


namespace App\Http\Controllers\Account;

use App\Events\AccountUpgradeEvent;
use App\Node;
use App\Package;
use App\User;
use App\Code;
use Illuminate\Http\Request;

class AccountUpgradeController
{

    public $account;

    public function __construct(Node $node)
    {
        $this->account = $node;
    }

    /**
     * @param $accountId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($accountId) {

        $canUpgrade = $this->canUpgrade($accountId);
        $account = $this->account->with(['package'])->where('id',$accountId)->first();

        $business = $this->hasPackageCode($accountId,2);
        $advance = $this->hasPackageCode($accountId,3);

        return view('accounts.upgrade', compact('account','canUpgrade','business','advance'));

    }

    public function upgrade(Request $request, $accountId) {

        $account = Node::find($accountId);
        $accountUser = User::find($account->user_id);
        $package = Package::where('code',$request->packageType)->first();

        if(!$this->hasPackageCode($accountId, $package->id))  {
            return response()->json([
                'success' => false,
                'message' => "Insufficient valid code for this package."
            ],422);
        }

        event(new AccountUpgradeEvent($accountUser,$account,$package));

        return response()->json([
            'success' => true,
            'message' => "Account <b>{$account->name} </b>has been upgraded."
        ],200);

    }


    public function canUpgrade($accountId) {


        if(!$this->haveUpgradeCode($accountId)) return false;
        if(!$this->isNotBusinessAccount($accountId)) return false;

        return true;
    }

    public function isNotBusinessAccount($accountId) {

        if(!in_array($this->account->with(['package'])->where('id',$accountId)->first(),['BP']))
            return true;

        return false;

    }

    /**
     * @param $accountId
     * @return integer
     */
    public function haveUpgradeCode($accountId)
    {
        $account = Node::with(['user'])->where('id',$accountId)->first();

        $codes = $account->user()
            ->with('codes')
            ->first()->codes()
            ->where('package_id', 2)
            ->where('status', 'VALID')
            ->count();

        if($codes > 0) return true;

        return false;
    }


    public function hasPackageCode($accountId, $packageCode)
    {
        $account = Node::with(['user'])->where('id',$accountId)->first();

        $codes = $account->user()
            ->with('codes')
            ->first()->codes()
            ->where('package_id', $packageCode)
            ->where('status', 'VALID')
            ->count();

        if($codes > 0) return true;

        return false;
    }

}