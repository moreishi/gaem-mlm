<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Wallet;
use App\User;
use App\Code;
use App\Node;
use Auth;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('wallet.index',[
            'summary' => $this->summary()
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transactions()
    {
        $transactions = Wallet::where(['user_id' => Auth::user()->id])->limit(15)->orderBy('id','DESC')->paginate(7);

        return view('wallet.transaction',[
            'transactions' => $transactions
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function withdraw(Request $request)
    {

        if($request->ajax()) {

            if($this->isTodayTimeNoon()) {
                return response()->json(['message' => 'You are now allowed to withdraw at the moment']);
            }

            $request->validate([
                'amount' => 'required|numeric|min:'.config('withdraw.minimum.wallet'),
                'remittance_service' => 'required|string',
            ]);

            $cash = $request->amount;
            $tax = ($cash / $this->get_tax());
            $processing = $this->get_processing_fee();
            $fees = $tax + $processing;
            $withdrawable = $cash - $fees;

            $available_cash = $this->cashout() - $this->cashout_request();

            if($cash > $available_cash)
                return response()->json(['message' => 'Your requested payout amount is way too high.'],422);

            $username = Auth::user()->username;

            $_data = [
                'amount' => $withdrawable,
                'fees' => $fees,
                'remittance_service' => $request->remittance_service,
                'payout_details' => $request->payout_details
            ];

            Wallet::create([
                'source' => 'cashout',
                'status' => 'PROCESSING',
                'type' => 'Dr',
                'description' => 'PAYOUT REQUEST - ' . $username,
                'currency' => 'PHP',
                'amount' => $cash,
                'data' => json_encode($_data),
                'user_id' => Auth::user()->id
            ]);


            return response()->json(['message' => 'Withdrawal complete']);
        }

        return view('wallet.withdraw', [
            'cashout' => $this->cashout() - $this->cashout_request(),
            'tax' => $this->get_tax(),
            'fee' => $this->get_processing_fee(),
            'minimum' => $this->get_minimum('wallet'),
            'form_open' => !$this->isTodayTimeNoon()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function total_fees($cash) {
        $tax = $cash / config('withdraw.fees.tax.percent');
        $fees = config('withdraw.fees.processing');
        return $tax + $fees;
    }

    public function credit() {
        return Wallet::where(['user_id' => Auth::user()->id,'type' => 'Cr'])->sum('amount');
    }

    public function debit() {
        return Wallet::where(['user_id' => Auth::user()->id,'type' => 'Dr'])->sum('amount');
    }

    public function balance() {
        return $this->credit() - $this->debit();
    }

    public function bonus() {
        return Wallet::where(['user_id' => Auth::user()->id,'source' => 'bonus'])->sum('amount');
    }

    public function pairing() {
        return Wallet::where(['user_id' => Auth::user()->id])
            ->where('description','LIKE','%MATCH COMMISSION%')
            ->sum('amount');
    }

    public function referral() {
        return  $referral = Wallet::where(['user_id' => Auth::user()->id])
            ->where('description','LIKE','%REFERRAL COMMISSION%')
            ->sum('amount');
    }

    public function unilevel() {
        return Wallet::where(['user_id' => Auth::user()->id])
            ->where('description','LIKE','%UNILEVEL COMMISSION%')
            ->sum('amount');
    }

    public function product() {
        return Wallet::where(['user_id' => Auth::user()->id,'source' => 'product'])->sum('amount');
    }

    public function payouts() {
        return Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'status' => 'COMPLETED',
            'type' => 'Dr'])->sum('amount');
    }

    public function travelFunds() {
        return Wallet::where(['user_id' => Auth::user()->id,'source' => 'travel','status' => 'COMPLETED'])->sum('amount');
    }

    public function followers() {
        return User::where('parent_id',Auth::user()->id)->count();
    }

    public function codes() {
        return Code::where(['user_id' => Auth::user()->id,'status' => 'VALID'])->count();
    }

    public function cashout() {
        $total = 0;

        $total += Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'status' => 'PROCESSING',
            'type' => 'Cr'])
            ->sum('amount');

        $total += Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'description' => 'MOVE TO MAIN WALLET',
            'status' => 'COMPLETED'])
            ->sum('amount');

        return $total;
    }

    public function cashout_request() {
        $total = 0;

        $total += Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'status' => 'PROCESSING',
            'type' => 'Dr'])
            ->sum('amount');

        $total += Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'status' => 'COMPLETED',
            'type' => 'Dr'])
            ->sum('amount');

        return $total;
    }

    public function royalty() {
        return Wallet::where(['user_id' => Auth::user()->id])
            ->where('description','LIKE','%ROYALTY COMMISSION%')
            ->sum('amount');
    }

    public static function total_commission() {
        return Wallet::where(['type' => 'Cr','user_id' => Auth::user()->id])->sum('amount');
    }

    public function summary() {
        $credit = $this->credit();
        $debit = $this->debit();
        $balance = $this->balance();
        $bonus = $this->bonus();
        $pairing = $this->pairing();
        $invites = $this->referral();
        $unilevel = $this->unilevel();
        $product = $this->product();
        $payouts = $this->payouts();
        $followers = $this->followers();
        $codes = $this->codes();
        $travel = $this->travelFunds();
        $cashout_request = $this->cashout_request();
        $cashout = $this->cashout() - $cashout_request;
        $royalty = $this->royalty();
        $total_commission = $this->total_commission();

        return compact('followers','credit','debit','balance','royalty',
            'bonus','pairing','invites','unilevel','product','payouts','codes','travel','cashout','total_commission');

    }

    public function get_minimum($wallet) {
        $amount = 0;
        if($wallet == 'pairing') $amount = config('withdraw.minimum.pairing');
        if($wallet == 'royalty') $amount = config('withdraw.minimum.royalty');
        if($wallet == 'referral') $amount = config('withdraw.minimum.referral');
        if($wallet == 'unilevel') $amount = config('withdraw.minimum.unilevel');
        if($wallet == 'main-wallet') $amount = config('withdraw.minimum.wallet');
        return $amount;
    }

    public function get_tax() {
        return config('withdraw.fees.tax.percent');
    }

    public function get_processing_fee() {
        return config('withdraw.fees.processing');
    }

    public function withdraw_process() {

        $withdrawals = Wallet::where([
            'type' => 'Dr',
//            'status' => 'PROCESSING',
            'source' => 'cashout'])->orderBy('id','DESC')->paginate(15);

        return view('wallet.withdraw_process',[
            'withdrawals' => $withdrawals
        ]);
    }

    public function withdraw_process_show($id) {
        $details = Wallet::where('id',$id)->with('user')->first();
        $data = json_decode($details->data);

        return view('wallet.withdraw_process_show',[
            'details' => $details,
            'additional_data' => $data
        ]);
    }

    public function withdraw_process_complete(Request $request) {

        $request->validate([
            'wallet_id' => 'required|numeric',
            'receipt_details' => 'required|string',
        ]);

        if(!auth()->user()->hasAnyRole(['admin','manager'])) {
            return response()->json(['message' => 'Not enough permission to continue.'],422);
        }

        $wallet = Wallet::where(['id' => $request->wallet_id])->first();

        if($wallet) {

            $wallet_data = json_decode($wallet->data);
            $wallet_data->remittance = [
                "details" => $request->receipt_details
            ];

            $wallet->data = json_encode($wallet_data);
            $wallet->update(['status' => 'COMPLETED']);

            return response()->json(['message' => 'Request has been completely processed.']);
        }

    }

    // Check if today's time is after noon
    public function isTodayTimeNoon() {

        $start = Carbon::now();
        $end = Carbon::now();

        $start->hour = 12;
        $start->minute = 00;
        $start->second = 00;

        $end->hour = 23;
        $end->minute = 59;
        $end->second = 59;

        return Carbon::now()->between($start,$end);

    }

}
