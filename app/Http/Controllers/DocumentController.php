<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Image;
use Auth;
use App\Document;

class DocumentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $documents = $this->documents($request);
        $search = '';
        return view('documents.index',compact('documents','search'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verification()
    {

        $frontId = $this->boolFront();
        $backId = $this->boolback();
        $selfieId = $this->boolSelfie();

        return view('documents.verification',[
            'frontId' => $frontId,
            'backId' => $backId,
            'selfieId' => $selfieId,
        ]);
    }

    public function upload(Request $request) {

        $request->validate([
            'fileDocument' => 'required',
            'fileLabel' => 'required'
        ]);


        if($request->fileDocument) {

            if($this->check_mime_image($request->fileDocument))
                return response()->json(['message','Unable to upload document'], 422);

            $pos  = strpos($request->fileDocument, ';');
            $type = str_replace('image/','',explode(':', substr($request->fileDocument, 0, $pos))[1]);
            $filename = time().'.'.$type;
            Image::make($request->fileDocument)->save(public_path('document/'.$filename));

            $filepath = config('app.APP_URL') . '/document/'.$filename;

            Document::create([
                'user_id' => Auth::user()->id,
                'type' => $this->documentType($request->fileLabel),
                'status' => 'PENDING',
                'path' => $filepath,
                'description' => $this->documentDescription($request->fileLabel)
            ]);

            return response()->json(['message' => 'Document has been uploaded']);
        }

        return response()->json(['message' => 'Unable to upload document'],422);

    }

    protected function check_mime_image($type) {
        if($type == 'image/jpg') return true;
        if($type == 'image/jpeg') return true;
        if($type == 'image/png') return true;
        if($type == 'image/gif') return true;
        return false;
    }

    public function documentType($fileLabel) {
        $type = '';
        if($fileLabel == 'FRONT') $type = 'FID';
        if($fileLabel == 'BACK') $type = 'BID';
        if($fileLabel == 'SELFIE') $type = 'SI';
        return $type;
    }

    public function documentDescription($fileDescription) {
        if($fileDescription == 'FRONT') return 'FRONT OF THE ID';
        if($fileDescription == 'BACK') return 'BACK OF THE ID';
        if($fileDescription == 'SELFIE') return 'SELF PHOTO WITH ID';
    }

    public function boolFront() {
        if(Document::where(['user_id' => Auth::user()->id,'status' => 'PENDING','type' => 'FID'])->count()) return true;
        return false;
    }

    public function boolBack() {
        if(Document::where(['user_id' => Auth::user()->id,'status' => 'PENDING','type' => 'BID'])->count()) return true;
        return false;
    }

    public function boolSelfie() {
        if(Document::where(['user_id' => Auth::user()->id,'status' => 'PENDING','type' => 'SI'])->count()) return true;
        return false;
    }

    public function documents(Request $request) {

        $documents = Document::with('user')
            ->whereHas('user', function($q) use ($request) {
                $q->where('firstname','LIKE',"%$request->search%")
                    ->orWhere('lastname','LIKE',"%$request->search%")
                    ->orWhere('phone','LIKE',"%$request->search%")
                    ->orWhere('username','LIKE',"%$request->search%");
            })->where(['status' => 'PENDING'])->paginate(3);

        return $documents;
    }

    public function approve($id) {

        if(Auth::user()->hasAnyRole(['admin','manager'])) {
            $document = Document::find($id);
            $document->status = 'APPROVED';
            $document->save();
            return response()->json(['message' => 'ID approved']);
        }

        return response()->json(['message' => "Restricted access."]);
    }

    public function deny($id) {

        if(Auth::user()->hasAnyRole(['admin','manager'])) {
            $document = Document::find($id);
            $document->status = 'DENIED';
            $document->save();
            return response()->json(['message' => 'ID approved']);
        }

        return response()->json(['message' => "Restricted access."]);
    }

    public function search(Request $request) {
        
        $documents = Document::with('user')
            ->whereHas('user', function($q) use ($request) {
                $q->where('firstname','LIKE',"%$request->search%")
                    ->orWhere('lastname','LIKE',"%$request->search%")
                    ->orWhere('phone','LIKE',"%$request->search%")
                    ->orWhere('username','LIKE',"%$request->search%");
            })->paginate(3);

        $documents->appends(['search' => $request->search]);

        $search = $request->search;

        return view('documents.index',compact('documents','search'));
    }

}
