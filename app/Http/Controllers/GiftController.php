<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Gift;
use App\User;
use App\Journal;
use Auth;

class GiftController extends Controller
{

//    public $user;

//    public $api_user;

    public function __construct()
    {
//        $this->user = Auth::user();
//        $this->api_user = auth('api')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gifts = Gift::where(['user_id' => Auth::user()->id,'status' => 'VALID'])->count();
        return view('gifts.index',['gifts' => $gifts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function transfer(Request $request) {

        if($request->ajax()) {
            $request->validate([
                'username' => 'required|exists:users,username',
                'quantity' => 'required|numeric|max:10',
            ]);
            $this->move($request);
        }

        return view('gifts.transfer');
    }

    // Mode gift codes
    public function move(Request $request) {
        $user = User::where('username',$request->username)->first();
        $gifts = Gift::where('user_id',Auth::user()->id)->limit($request->quantity)->update([
            'user_id' => $user->id
        ]);

        Journal::create([
            'data' => json_encode([
                'model' => "Discount",
                "type" => "Transfer",
                "from" => ["user_id" => Auth::user()->id,'username' => Auth::user()->username],
                "to" => ["user_id" => $user->id,'username' => $user->username],
                "codes" => ["quantity" => $request->quantity]
            ])
        ]);
    }

}
