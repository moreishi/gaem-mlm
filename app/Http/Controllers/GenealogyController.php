<?php

namespace App\Http\Controllers;

use App\Code;
use App\Events\Jobs\CrawlGenealogyEvent;
use App\User;
use App\Package;
use App\Services\Genealogy\GenealogyService;
use Illuminate\Http\Request;
use App\Events\ActivateNode;
use App\Node;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Cache;

class GenealogyController extends Controller
{
    public $genealogyService;

    /**
     * GenealogyController constructor.
     * @param GenealogyService $genealogyService
     */
    public function __construct(GenealogyService $genealogyService)
    {
        $this->genealogyService = $genealogyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $node = Node::where('user_id',Auth::user()->id)->first();

        $accounts = Node::where('user_id',Auth::user()->id)->count();

        return view('genealogy.index',[
            'nodes' => $this->nodes($node->id),
            'accounts' => json_encode($accounts),
            'codes' => $this->get_codes()
        ]);
    }

    public function show($nodeName)
    {
        $node = Node::where('name',$nodeName)->first();
        $accounts = Node::where('user_id',Auth::user()->id)->count();

        if(!$node) return redirect()->route('genealogy');

        return view('genealogy.index',[
            'nodes' => $this->nodes($node->id),
            'codes' => $this->get_codes(),
            'accounts' => json_encode($accounts)
        ]);
    }

    public function nodes($id = null) {

        $placeholder = [];

        // id 1
        $root = Node::where('id',$id)->with('user')->first();

        //id 2,3
        $node_lv1_lft = $this->get_node(!empty($root) ? $root->lft : $placeholder);
        $node_lv1_rgt = $this->get_node(!empty($root) ? $root->rgt : $placeholder);

        // id 4,5,6,7
        $node_lv2_lft_lft = $this->get_node(!empty($node_lv1_lft) ? $node_lv1_lft->lft : $placeholder);
        $node_lv2_lft_rgt = $this->get_node(!empty($node_lv1_lft) ? $node_lv1_lft->rgt : $placeholder);
        $node_lv2_rgt_lft = $this->get_node(!empty($node_lv1_rgt) ? $node_lv1_rgt->lft : $placeholder);
        $node_lv2_rgt_rgt = $this->get_node(!empty($node_lv1_rgt) ? $node_lv1_rgt->rgt : $placeholder);

        // id 8,9,10,11,12,13,14,15
        $node_lv3_lft1_lft = $this->get_node(!empty($node_lv2_lft_lft) ? $node_lv2_lft_lft->lft : $placeholder);
        $node_lv3_lft1_rgt = $this->get_node(!empty($node_lv2_lft_lft) ? $node_lv2_lft_lft->rgt : $placeholder);
        $node_lv3_rgt1_lft = $this->get_node(!empty($node_lv2_lft_rgt) ? $node_lv2_lft_rgt->lft : $placeholder);
        $node_lv3_rgt1_rgt = $this->get_node(!empty($node_lv2_lft_rgt) ? $node_lv2_lft_rgt->rgt : $placeholder);
        $node_lv3_lft2_lft = $this->get_node(!empty($node_lv2_rgt_lft) ? $node_lv2_rgt_lft->lft : $placeholder);
        $node_lv3_lft2_rgt = $this->get_node(!empty($node_lv2_rgt_lft) ? $node_lv2_rgt_lft->rgt : $placeholder);
        $node_lv3_rgt2_lft = $this->get_node(!empty($node_lv2_rgt_rgt) ? $node_lv2_rgt_rgt->lft : $placeholder);
        $node_lv3_rgt2_rgt = $this->get_node(!empty($node_lv2_rgt_rgt) ? $node_lv2_rgt_rgt->rgt : $placeholder);

        return json_encode([
            'root' => $root,
            'nodeOne' => $node_lv1_lft,
            'nodeTwo' => $node_lv1_rgt,
            'nodeThree' => $node_lv2_lft_lft,
            'nodeFour' => $node_lv2_lft_rgt,
            'nodeFive' => $node_lv2_rgt_lft,
            'nodeSix' => $node_lv2_rgt_rgt,
            'nodeSeven' => $node_lv3_lft1_lft,
            'nodeEight' => $node_lv3_lft1_rgt,
            'nodeNine' => $node_lv3_rgt1_lft,
            'nodeTen' => $node_lv3_rgt1_rgt,
            'nodeEleven' => $node_lv3_lft2_lft,
            'nodeTwelve' => $node_lv3_lft2_rgt,
            'nodeThirteen' => $node_lv3_rgt2_lft,
            'nodeFourteen' => $node_lv3_rgt2_rgt,
        ]);

    }

    public function get_node($nodeId = null) {
        $arrNode = [];
        if($nodeId == null || $nodeId == "" || empty($nodeId)) return $arrNode;
        $_node = Node::where('id',$nodeId)->with('user')->first();
        if($_node) $arrNode = $_node;
        return $arrNode;
    }

    private function avatars($nodeName = null) {

        $array = Node::with('user')->where('user_id',Auth::user()->id)->first()->descendantsAndSelf()->get();

        if( !$nodeName == null ) {
            $array = Node::with('user')->where('name',$nodeName)->first()->descendantsAndSelf()->get();
        }

        $avatars = [];

        foreach($array as $key => $value) {
            $avatar = User::where('id',$value->user_id)->first()->avatar;
            array_push($avatars,['id' => $value->id,'avatar' => $avatar]);
        }

        return json_encode($avatars);
    }


    public function levelOne($id = null) {
        return Node::find($id)->with(['child_lft','child_rgt']);
    }

    public function store(Request $request) {

        $request->validate([
            'position' => 'required|string',
            'nodeId' => 'required|numeric',
            'package_id' => 'required|numeric'
        ]);

        $user_accounts_counts = Node::where('user_id', Auth::user()->id)->count();

        if($user_accounts_counts >= 7)
            return response()->json(['message' => 'Account limit exceeded!'],422);

        $count = Code::where([
            'user_id' => Auth::user()->id,
            'package_id' => $request->package_id,
            'status' => 'VALID'])->count();

        $leg_null = Node::where(['id' => $request->nodeId])->first();

        if($count < 1) return response()->json(['message' => 'Unable to process request'],422);
        if($leg_null[$request->position] != NULL) return response()->json(['message' => 'Unable to process request'],422);

        $package = Package::find($request->package_id);

        $attr = [
            'user_id' => Auth::user()->id,
            'parent_id' => $request->nodeId,
            'package_id' => $request->package_id,
            'position' => $request->position,
            'points' => $this->get_points($request->package_id),
            'name' => 'Acct_'.time()];

        $node = $node = Node::create($attr);

        event(new ActivateNode(Auth::user(),$package,$attr,$node));
        event(new CrawlGenealogyEvent($node));

        return response()->json(['message' => 'Node has been created', $request->position => $leg_null[$request->position]]);

    }

    private function get_codes() {

        $codes = ['SP' => 0, 'BP' => 0, 'AP' => 0, 'total' => 0];

        $codes['SP'] = Code::where(['user_id' => Auth::user()->id, 'package_id' => 1])->count();
        $codes['BP'] = Code::where(['user_id' => Auth::user()->id, 'package_id' => 2])->count();
        $codes['AP'] = Code::where(['user_id' => Auth::user()->id, 'package_id' => 3])->count();
        $codes['total'] =  (int) $codes['SP'] + (int) $codes['BP'] + (int) $codes['AP'];


        return json_encode($codes);
    }

    public function add_node($arr = []) {
        $node = Node::create($arr);
        $parent = Node::find($arr['parent_id']);

        if($arr['position'] == 'lft') $parent->setLft($node->id);
        if($arr['position'] == 'rgt') $parent->setRgt($node->id);

        return $this;
    }

    protected function get_points($package_id) {
        \Log::info('test package', ['package' => $package_id]);

        $package_code = Package::find($package_id)->code;

        if($package_code == 'SP') return config('packages.SP.points');
        if($package_code == 'BP') return config('packages.BP.points');
    }

    public function convert_date() {
        $matched = json_encode([
            'match' => [
                'count' => 0,
                'updated_at' => Carbon::now()
            ]
        ]);

        \Log::info('count',['match' => $matched]);

        $matched = json_decode($matched);

    }

    public function search()
    {
        $genealogies = GenealogyService::get(Auth::user()->id);

        return view('genealogy.search', compact('genealogies'));
    }

}
