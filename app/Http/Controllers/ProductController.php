<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index',[
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update_products()
    {

        $longveggiety = Product::where('sku','longveggiety')->first();
        if(!$longveggiety) {
            Product::create([
                'name' => 'Longveggiety',
                'sku' => 'longveggiety',
                'currency' => 'PHP',
                'regular_price' => 399,
                'sale_price' => 399,
                'pv' => 80,
            ]);
        }

        $perfume = Product::where('sku','perfume')->first();
        if(!$perfume) {
            Product::create([
                'name' => 'GAEM Perfume',
                'sku' => 'perfume',
                'currency' => 'PHP',
                'regular_price' => 325,
                'sale_price' => 325,
                'pv' => 80,
            ]);
        }

        $scalar = Product::where('sku','scalar')->first();
        if($scalar) {
            $scalar->regular_price = 199;
            $scalar->sale_price = 199;
            $scalar->pv = 40;
            $scalar->save();
        }

        $scalar = Product::where('sku','vaporeen')->first();
        if($scalar) {
            $scalar->name = 'GAEM Fresh';
            $scalar->sku = 'gaem_fresh';
            $scalar->regular_price = 199;
            $scalar->sale_price = 199;
            $scalar->pv = 20;
            $scalar->save();
        }

        $scalar = Product::where('sku','mega_rub')->first();
        if($scalar) {
            $scalar->regular_price = 125;
            $scalar->sale_price = 125;
            $scalar->pv = 20;
            $scalar->save();
        }

        $scalar = Product::where('sku','coffee')->first();
        if($scalar) {
            $scalar->regular_price = 240;
            $scalar->sale_price = 240;
            $scalar->pv = 40;
            $scalar->save();
        }

        $scalar = Product::where('sku','perfume')->first();
        if($scalar) {
            $scalar->regular_price = 325;
            $scalar->sale_price = 325;
            $scalar->pv = 40;
            $scalar->save();
        }

        $scalar = Product::where('sku','longveggiety')->first();
        if($scalar) {
            $scalar->regular_price = 399;
            $scalar->sale_price = 399;
            $scalar->pv = 80;
            $scalar->save();
        }

        return response()->json([
            'message' => 'Data has been updated.'
        ], 200);

    }

}
