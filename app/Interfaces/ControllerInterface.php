<?php


namespace App\Interfaces;

use Illuminate\Http\Request;

/**
 * Interface ControllerInterface
 * @package App\Interfaces
 */
interface ControllerInterface
{
    /**
     * @return mixed
     */
    public function index();

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);

    /**
     * @param $id
     * @return mixed
     */
    public function show($id);

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request);

    /**
     * @return mixed
     */
    public function create();

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);
}