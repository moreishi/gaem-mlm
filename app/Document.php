<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Document extends Model
{
    public $fillable = [
        'user_id','type','description','path','status'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

}
