<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{

    public $fillable = [
        'code','user_id','package_id','status'
    ];

    public function user() {
        return $this->belongsTo(App\User::class);
    }

    public function package() {
        return $this->belongsTo(\App\Package::class);
    }

    public function totalCount() {
        return $this->where('status','VALID');
    }

}