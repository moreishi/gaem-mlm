<?php
/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-02-23
 * Time: 23:25
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genealogy extends Model
{

    public $table = "nodes";

    public $fillable = ['name','lft','rgt','parent_id','user_id'];

    public function setLft($id = null) {
        return $this->update(['lft' => $id]);
    }

    public function setRgt($id = null) {
        return $this->update(['rgt' => $id]);
    }

    public function child_lft() {
        return $this->hasMany('App\Genealogy','lft','id');
    }

    public function child_rgt() {
        return $this->hasMany('App\Genealogy','rgt','id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

}