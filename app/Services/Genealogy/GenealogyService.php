<?php


namespace App\Services\Genealogy;

use Illuminate\Support\Facades\Cache;
use App\User;
use App\Node;
use Log;

/**
 * The purpose of this class is to store all new accounts
 * to geanealogy tree of the parent node account.
 *
 * Class GenealogyService
 * @package App\Services\Genealogy
 */
class GenealogyService
{
    public $user;

    public $node;

    public $baseKey;

    public $processed_users = [];

    public $app_name;

    /**
     * GenealogyService constructor.
     * @param Node $node
     */
    public function __construct(Node $node)
    {
        $this->node = $node;

        $this->app_name = config('DB_DATABASE');

        $this->baseKey = "{$this->app_name}_GENEALOGY_TREE";
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public static function get($user_id)
    {
        $app = config('DB_DATABASE')."_GENEALOGY_TREE";
        return Cache::rememberForever("{$app}_USER_ID_{$user_id}", function() {
            return [];
        });
    }

    /**
     * Check node if already enlisted.
     * @param Node $node
     * @return mixed
     */
    private function isEnlisted(Node $node)
    {
        return Cache::rememberForever("{$this->baseKey}_NODE_ID_{$node->id}_ENLISTED", function() {
            return false;
        });
    }

    /**
     * Node will be added to listed CACHE
     * @param Node $node
     * @param bool $boolean
     */
    protected function setEnlist(Node $node, $boolean =  false)
    {
        Cache::forever("{$this->baseKey}_NODE_ID_{$node->id}_ENLISTED", $boolean);
    }

    /**
     *
     */
    public function store()
    {
        /*
         * Check if node is enlisted.
         */
        if($this->isEnlisted($this->node)) return;

        $this->process($this->node);
    }

    /**
     * @param $node
     * @param $counter
     */
    private function process($node, $counter = 1)
    {
        /*
         * Get parent node.
         */
        $parent_node = Node::find($node->parent_id);

        /*
         * If no parent node available mark new node as enlisted and then terminate the process.
         */
        if(!$parent_node)
        {
            $this->setEnlist($node, true);

            return;
        }

        /*
         * Get parent node user.
         */
        $user = User::find($parent_node->user_id);

        // array_push($this->processed_users,$user);

        /*
         * Save last processed node.
         */
        //$this->process_node($node);

        /*
         * Increment level process.
         */
        //$this->process_node_level($counter);

        /*
         * Add to user genealogy list.
         */
        $this->push($user, $this->node);

        /*
         * Process again until no node is available.
         */
        $this->process($parent_node, $counter + 1);
    }

    /**
     * @param $node
     */
    public function process_node($node)
    {
        Cache::forever("{$this->baseKey}_LAST_NODE_PROCESSED",$node);
    }

    public function process_node_level($counter)
    {
        Cache::forever("{$this->baseKey}_LAST_NODE_PROCESSED_LEVEL",["level" => $counter,"users" => $this->processed_users]);
    }

    /**
     * Global
     */
    public function get_last_processed_node()
    {
        return Cache::get("{$this->baseKey}_LAST_NODE_PROCESSED");
    }

    /**
     * @return mixed
     */
    public function get_process_node_level()
    {
        return Cache::get("{$this->baseKey}_LAST_NODE_PROCESSED_LEVEL");
    }

    /**
     * @param $user
     * @param $node
     */
    private function push($user, $node)
    {
        $list = Cache::rememberForever("{$this->baseKey}_USER_ID_{$user->id}",function() {
            return [];
        });

        $_exist = false;

        foreach ($list as $key => $value)
        {
            if($value->id == $node->id) {
                $_exist = true;
            }
        }

        if(!$_exist) {
            array_push($list, $node);
            Cache::forever("{$this->baseKey}_USER_ID_{$user->id}",$list);
        }
    }

}