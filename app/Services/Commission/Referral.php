<?php


namespace App\Services\Commission;


use App\Node;
use App\User;
use App\Gift;
use App\Wallet;
use Illuminate\Support\Facades\Log;


class Referral
{

    public $node;

    public $user;

    public function __construct(User $user, Node $node)
    {

        $this->user = $user;

        $this->node = $node;

        $this->dispatch_commission($this->user, $this->node);

    }


    public function dispatch_commission($node, $user)
    {
        // Current Node Package Id
        $packageId = $node->package_id;

        // Parent of current NODE user.
        $sponsorId = $node->user->parent_id;

        if ((int)$packageId == 1) {

            $discountCodeValue = config('commisions.referral.SP.gc.value');
            $discountCodeQuantity = config('commisions.referral.SP.gc.quantity');

            $this->add_gift_certificate($discountCodeQuantity, $sponsorId, $discountCodeValue);

        }
        if ((int)$packageId == 2) {

            $cash = config('bonus.referral_commission.BP.cash');
            $dc_value = config('bonus.referral_commission.BP.gc.value');
            $dc_quantity = config('bonus.referral_commission.BP.gc.quantity');

            $this->add_gift_certificate($dc_quantity, $sponsorId, $dc_value);

            Wallet::create([
                'source' => 'direct-invite',
                'description' => 'REFERRAL COMMISSION',
                'amount' => $cash,
                'type' => 'Cr',
                'currency' => "PHP",
                'status' => "COMPLETED",
                'user_id' => $sponsorId]);

        }

    }

    public function add_gift_certificate($discountCodeQuantity, $sponsorId, $discountCodeValue)
    {
        for($i = 0; $i < $discountCodeQuantity; $i++) {
            Gift::create([
                'amount' => $discountCodeValue,
                'user_id' => $sponsorId,
                'code' => random(1000,9999).time(),
                'status' => 'VALID',
            ]);
        }
    }

}