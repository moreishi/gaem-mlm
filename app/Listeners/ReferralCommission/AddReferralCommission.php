<?php

namespace App\Listeners\ReferralCommission;

use App\Events\ReferralCommission;
use App\Package;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Node;
use App\Code;
use App\Gift;
use App\Wallet;

class AddReferralCommission
{

    public $user;

    public $node;

        /**
         * Create the event listener.
         *
         * @return void
         */
        public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ReferralCommission  $event
     * @return void
     */
    public function handle(ReferralCommission $event)
    {

        $this->user = $event->user;
        $this->node = Node::where('id',$event->node->id)->with('user')->first();

        $this->add_commission_sponsor($event->node->package_id,$this->node->user->parent_id);

    }

    public function get_ancestors_depth(Node $node, $depth = 0) {
        $this->get_ancestor($node,$depth,$node->package_id);
    }

    public function add_commission_sponsor($package_id,$sponsor_id) {

        if(!$sponsor_id) return;

        if ((int)$package_id == 1) {
            $dc_value = config('bonus.referral_commission.SP.gc.value');
            $dc_quantity = config('bonus.referral_commission.SP.gc.quantity');
            // factory(\App\Gift::class, $dc_quantity)->create(['amount' => $dc_value, 'user_id' => $sponsor_id]);

            for($i = 0; $i < $dc_quantity; $i++)
            {
                Gift::create([
                    'code' => rand(1000,9999).time(),
                    'amount' => $dc_value,
                    'user_id' => $sponsor_id,
                    'status' => 'VALID',
                ]);
            }

        }
        if ((int)$package_id == 2) {
            $cash = config('bonus.referral_commission.BP.cash');
            $dc_value = config('bonus.referral_commission.BP.gc.value');
            $dc_quantity = config('bonus.referral_commission.BP.gc.quantity');
            // factory(\App\Gift::class, $dc_quantity)->create(['amount' => $dc_value, 'user_id' => $sponsor_id]);

            for($i = 0; $i < $dc_quantity; $i++)
            {
                Gift::create([
                    'code' => rand(1000,9999).time(),
                    'amount' => $dc_value,
                    'user_id' => $sponsor_id,
                    'status' => 'VALID',
                ]);
            }

            Wallet::create([
                'source' => 'direct-invite',
                'description' => 'REFERRAL COMMISSION',
                'amount' => $cash,
                'type' => 'Cr',
                'currency' => "PHP",
                'status' => "COMPLETED",
                'user_id' => $sponsor_id
            ]);

//            factory(\App\Wallet::class, 1)->create([
//                'source' => 'direct-invite',
//                'description' => 'REFERRAL COMMISSION',
//                'amount' => $cash,
//                'type' => 'Cr',
//                'currency' => "PHP",
//                'status' => "COMPLETED",
//                'user_id' => $sponsor_id]);
        }
    }

}
