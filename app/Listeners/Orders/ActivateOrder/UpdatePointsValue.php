<?php

namespace App\Listeners\Orders\ActivateOrder;

use App\Events\Orders\ActivateOrder;
use App\Product;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Order;
use App\Wallet;
use App\User;
use Auth;
use Illuminate\Support\Facades\Log;

class UpdatePointsValue
{

    public $user;

    public $order;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateOrder  $event
     * @return void
     */
    public function handle(ActivateOrder $event)
    {
        $this->order = $event->order;

        $this->user = $event->user;

        $this->execute_points_value();
    }

    public function execute_points_value()
    {
        $depth = (int) config('commissions.unilevel.depth');

        $this->add_points_value($this->user,$this->order,$depth);

        $this->add_cash_from_global_pool($this->get_product_quantity($this->order) * 5);
    }

    public function add_points_value(User $user, Order $order, $depth) {

        if($depth < 1) return;

        $pv_date = Carbon::parse(json_decode($user->data)->updated_at);

        if($pv_date->format('m') != Carbon::today()->format('m')) {

            $data = json_decode($user->data);

            $data->activated = ($data->pv >= 160) ? true : false;
            $data->pool = ($data->pv >= 320) ? true : false;
            $data->pv = 0;
            $data->updated_at = Carbon::today()->format('Y-m-d H:i:s');

            $user->pv = 0; // current user receives points value
            $user->data = json_encode($data);
            $user->save();

        }

        if(json_decode($user->data)->activated == true || $user->id == Auth::user()->id) {

            $points = $this->compute_points_value($order);

            $data = json_decode($user->data);
            $data->pv += $points;
            $data->updated_at = Carbon::now()->format('Y-m-d H:i:s');

            $this->add_cash_from_pv($user->id,$points);
            $user->pv += $points; // current user receives points value
            $user->data = json_encode($data);
            $user->save();

            $this->update_order_status($order);

        }

        if($user->parent_id == NULL) return;

        $user_parent = User::find($user->parent_id);

        $count = $depth-1;

        $this->add_points_value($user_parent,$order,$count);

    }

    /**
     * Get points value.
     * @param Order $order
     * @return float|int
     */
    protected function compute_points_value(Order $order)
    {
        $points = 0;

        $products = json_decode($order->data, true);

        foreach ($products as $key => $value)
            $points += $this->get_stock_points_value($key) * (int) $products[$key];

        $points = ($points * (int) config('commissions.unilevel.commission.percent')) / 100;

        return $points;
    }

    /**
     * Get Points Value; The quantity of the products are same with Points Value.
     * @param $order
     * @return int
     */
    protected function get_product_quantity(Order $order) {

        $quantity = 0;

        foreach (array_values(json_decode($order->data,true)) as $qty) $quantity += (int) $qty;

        return $quantity;

    }

    protected function get_stock_points_value($sku)
    {
        return (float) Product::where('sku',$sku)->first()->pv;
    }

    protected function add_cash_from_pv($user_id,$amount) {
        return Wallet::create([
            'source' => 'unilevel',
            'amount' => $amount,
            'description' => 'UNILEVEL COMMISSION',
            'currency' => "PHP",
            'type' => 'Cr',
            'status' => 'COMPLETED',
            'user_id' => $user_id
        ]);
    }

    protected function add_cash_from_global_pool($amount) {
        return Wallet::create([
            'source' => 'global-pool',
            'amount' => $amount,
            'description' => 'GLOBAL POOL COMMISSION',
            'currency' => "PHP",
            'type' => 'Cr',
            'status' => 'COMPLETED'
        ]);
    }

    protected function update_order_status(Order $order) {
        $order->update(['status' => 'USED']);
    }

}
