<?php

namespace App\Listeners\Jobs;

use App\Events\Jobs\CrawlGenealogyEvent;
use App\Services\Genealogy\GenealogyService;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrawlGenealogyListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CrawlGenealogyEvent  $event
     * @return void
     */
    public function handle(CrawlGenealogyEvent $event)
    {
        $genealogy = new GenealogyService($event->node);
        $genealogy->store();
    }
}
