<?php

namespace App\Listeners\ActivateNode;

use App\Events\ActivateNode;
use App\Events\ReferralCommission;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Code;
use App\User;
use Auth;

class UpdateCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateNode  $event
     * @return void
     */
    public function handle(ActivateNode $event)
    {
        $this->update_code($event);
    }

    public function update_code($event) {
        $code = Code::where([
            'user_id' => $event->user->id,
            'package_id' => $event->package->id,
            'status' => 'VALID'])->first();
        $code->status = 'USED';
        $code->save();

    }

}
