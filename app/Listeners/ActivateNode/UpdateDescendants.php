<?php

namespace App\Listeners\ActivateNode;

use App\Events\ActivateNode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Node;

class UpdateDescendants
{

    public $node;

    public $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateNode  $event
     * @return void
     */
    public function handle(ActivateNode $event)
    {
        \Log::info('Descendants',['event' => $event]);
        $this->node = $event->node;
        $this->user = $event->user;
        $this->update_ancestors_count($this->node);
    }

    public function update_ancestors_count(Node $node) {
        \Log::info('Descendants',['node' => $node]);

        if(!$node->parent_id) return;

        $parent_node = $this->get_parent_node($node->parent_id);

        if($node->id == $parent_node->lft) {
            $parent_node->lcount++;
            $parent_node->save();
            \Log::info('Adding Left Count',['node' => $node]);
        } elseif ($node->id == $parent_node->rgt) {
            $parent_node->rcount++;
            $parent_node->save();
            \Log::info('Adding Right Count',['node' => $node]);
        }

        $this->update_ancestors_count($parent_node);

    }

    public function get_parent_node($parent_id) {
        return Node::find($parent_id);
    }

}
