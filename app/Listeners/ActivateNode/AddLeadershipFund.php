<?php

namespace App\Listeners\ActivateNode;

use App\Events\ActivateNode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Wallet;

class AddLeadershipFund
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ActivateNode  $event
     * @return void
     */
    public function handle(ActivateNode $event)
    {
        $this->add_leadership_fund($event->node);
    }

    // Leadership
    protected function add_leadership_fund($node) {
        $amount = $this->get_leadership_fund_value($node->package_id);
        $this->add_cash_to_wallet($amount);

    }

    protected function add_cash_to_wallet($amount) {
        Wallet::create([
            'type' => 'Cr',
            'description' => 'WEEKLY LEADERSHIP POINTS COMMISSION',
            'amount' => $amount,
            'status' => 'COMPLETED',
            'currency' => 'PHP',
            'source' => 'leadership'
        ]);
    }

    protected function get_leadership_fund_value($package_id) {
        $value = 0;
        if($package_id == 1) $value = 18;
        if($package_id == 2) $value = 28;
        if($package_id == 3) $value = 38;
        return $value;
    }

}
