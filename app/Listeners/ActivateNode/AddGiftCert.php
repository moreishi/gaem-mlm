<?php

namespace App\Listeners\ActivateNode;

use App\Events\ActivateNode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Gift;
use App\Package;
use Illuminate\Support\Facades\Log;

class AddGiftCert
{

    public $user;

    public $package;

    public $attr;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateNode  $event
     * @return void
     */
    public function handle(ActivateNode $event)
    {

        $this->user = $event->user;

        $this->package = $event->package;

        Log::info("NEW ACCOUNT TYPE",[$this->package,$this->user]);

        if($event->package->code == 'SP') {
            $gc_value = config('packages.SP.addOn.gc.value');
            $gc_quantity = config('packages.SP.addOn.gc.quantity');
            $this->dispatch_gift_certificate($event->attr['user_id'], $gc_quantity, $gc_value);
        }
        if($event->package->code == 'BP') {
            $gc_value = config('packages.BP.addOn.gc.value');
            $gc_quantity = config('packages.BP.addOn.gc.quantity');
            $this->dispatch_gift_certificate($event->attr['user_id'], $gc_quantity, $gc_value);
        }
        if($event->package->code == 'AP') {
            $gc_value = config('packages.AP.addOn.gc.value');
            $gc_quantity = config('packages.AP.addOn.gc.quantity');
            $this->dispatch_gift_certificate($event->attr['user_id'], $gc_quantity, $gc_value);
        }

    }

    public function dispatch_gift_certificate($user_id, $quantity, $value)
    {
        for($i = 0; $i < $quantity; $i++) {
            Gift::create([
                'code' => random_int(1000,9999).time(),
                'status' => 'VALID',
                'amount' => $value,
                'user_id' =>$user_id
            ]);
            Log::info("GIFT_CARD",[$user_id, $quantity, $value]);
        }
    }

}
