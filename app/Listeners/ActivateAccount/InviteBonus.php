<?php

namespace App\Listeners\ActivateAccount;

use App\Events\ActivateAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use App\Role;
use App\User;

class InviteBonus
{

    public $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateAccount  $event
     * @return void
     */
    public function handle(ActivateAccount $event)
    {
        Log::info('Invite Bonus', [$event->user]);
        $this->user = $event->user;
        $this->process_bonus($this->user,(int) config('bonus.direct.depth'));
    }

    public function process_bonus(User $user,$depth) {

        // Depth ancestor level

        if ($depth > 0) {
            $depth--;
            $parent = User::where('id',$user->parent_id)->first();

            if(!$parent->hasRole('inactive')) {
                Log::info('Giving Bonus', ['id' => $parent->id, 'username' => $parent->username]);
            }

            if($parent->parent_id) {
                $this->process_bonus($parent, $depth);
            }


            
        }

    }



}
