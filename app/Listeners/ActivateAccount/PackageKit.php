<?php

namespace App\Listeners\ActivateAccount;

use App\Events\ActivateAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Package;
use App\Gift;
use App\Node;
use Log;

class PackageKit
{

    public $user;
    public $package;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateAccount  $event
     * @return void
     */
    public function handle(ActivateAccount $event)
    {
        $this->package = $event->package;
        $this->user = $event->user;

        if($event->package->code == 'SP') {
            $gc_value = config('packages.SP.addOn.gc.value');
            $gc_quantity = config('packages.SP.addOn.gc.quantity');
            factory(Gift::class,$gc_quantity)->create(['user_id' => $event->user->id,'amount' => $gc_value]);
        }
        if($event->package->code == 'BP') {
            $gc_value = config('packages.BP.addOn.gc.value');
            $gc_quantity = config('packages.BP.addOn.gc.quantity');
            factory(Gift::class,$gc_quantity)->create(['user_id' => $event->user->id,'amount' => $gc_value]);
        }
        if($event->package->code == 'AP') {
            $gc_value = config('packages.AP.addOn.gc.value');
            $gc_quantity = config('packages.BP.addOn.gc.quantity');
            factory(Gift::class,$gc_quantity)->create(['user_id' => $event->user->id,'amount' => $gc_value]);
        }


    }



}
