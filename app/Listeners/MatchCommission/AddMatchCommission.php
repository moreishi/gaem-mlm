<?php

namespace App\Listeners\MatchCommission;

use App\Events\MatchCommission;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddMatchCommission
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MatchCommission  $event
     * @return void
     */
    public function handle(MatchCommission $event)
    {
        //
    }
}
