<?php

namespace App\Listeners\AccountUpgrade;

use App\Events\AccountUpgradeEvent;
use App\Events\TravelFund;
use App\Node;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AccountUpdateDescendantBonusListener
{

    public $user;

    public $node;

    public $package;

    private $dateToday;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->dateToday = Carbon::today();
    }

    /**
     * Handle the event.
     *
     * @param  AccountUpgradeEvent  $event
     * @return void
     */
    public function handle(AccountUpgradeEvent $event)
    {

        $this->user = $event->user;

        $this->node = $event->node;

        $this->package = $event->package;

        $this->process_points($this->node,$this->get_points($this->package->id));

    }

    // $node newly created; $points to add to ancestors
    public function process_points(Node $node, $points) {

        if($node->parent_id == NULL) return;

        $parent_node = Node::find($node->parent_id);

        $olft = $parent_node->lft_pts;
        $orgt = $parent_node->rgt_pts;

        if( $node->id == $parent_node->lft ) {

            if($parent_node->rgt_pts == 0) {

                $parent_node->lft_pts += $points;
                $parent_node->save();

            } elseif($parent_node->rgt_pts > 0) {

                // new rpoint value
                $rpoints = $points - $parent_node->rgt_pts;
                if($rpoints < 0) $rpoints = 0;

                // current node remaining poitns to add
                $lpoints = $parent_node->rgt_pts - $points;
                if($parent_node->rgt_pts - $points < 0) $lpoints = 0;

                $parent_node->lft_pts = $rpoints;
                $parent_node->rgt_pts = $lpoints;
                $parent_node->save();

                $this->add_match_count($parent_node,($orgt - $lpoints));

            }

        }

        if( $node->id == $parent_node->rgt ) {

            if($parent_node->lft_pts == 0) {

                $parent_node->rgt_pts += $points;
                $parent_node->save();

            } elseif($parent_node->lft_pts > 0) {

                // new rpoint value
                $lpoints = $points - $parent_node->lft_pts;
                if($lpoints < 0) $lpoints = 0;

                // current node remaining points to add
                $rpoints = $parent_node->lft_pts - $points;
                if($parent_node->lft_pts - $points < 0) $rpoints = 0;

                $parent_node->lft_pts = $rpoints;
                $parent_node->rgt_pts = $lpoints;
                $parent_node->save();

                $this->add_match_count($parent_node,($olft - $rpoints));

            }
        }

        $this->process_points($parent_node,$points);

    }

    // verify match points max daily
    public function isMaxMatched(Node $node,$max = 0) {
        $data = json_decode($node->data);
        if($max > $data->match->count) return false;
        return true;
    }

    public function get_node_match_count(Node $node) {
        $data = json_decode($node->data);
        return $data->match->count;
    }

    public function add_match_count(Node $node,$points = 0) {

        if($this->get_node_match_count($node) > $this->get_max_matched($node->package_id)) return;

        $data = json_decode($node->data);

        while($points > 0) {

            if(!$this->isDateToday($data->match->updated_at->date)) {
                $data->match->count = 0;
                $data->match->updated_at->date = $this->dateToday;
            }

            if($data->match->count < $this->get_max_matched($node->package_id)) {
                $data->match->count++;
            }
            if($data->match->count % 5 == 0) {
                event(new TravelFund($node,User::find($node->user->id)));
            } else {

                $this->add_cash($node, 100);
                $this->add_royalty($node, 10);
                $node->balance += 100;

            }
            $points--;
        }

        // This is a workaround fix
        $data_json_encode = json_encode($data);
        $data_json_decode = json_decode($data_json_encode, true);

        if(isset($data_json_decode['match']['updated_at']['date']['date'])) {
            $data_json_decode['match']['updated_at'] = $data_json_decode['match']['updated_at']['date'];
        } else {
            $data_json_decode['match']['updated_at'] = $data_json_decode['match']['updated_at'];
        }
        // End Of Quick Around Fix

        $node->data = json_encode($data_json_decode);

        $node->save();
    }

    public function reduceValue($value = 0 ,$points = 0) {
        if($value == 0) return $value;
        while ($points > 0) {
            if( $value == 0 && $points > 0 ) {
                return $points;
            }
            $value--;
            $points--;
        }
        return $value;
    }

    public function add_cash(Node $node, $amount = 0) {
        return Wallet::create([
            'source' => 'pairing',
            'description' => 'MATCH COMMISSION',
            'amount' => $amount,
            'type' => 'Cr',
            'currency' => "PHP",
            'status' => "COMPLETED",
            'user_id' => $node->user->id,
            'node_id' => $node->id
        ]);
    }

    public function add_royalty(Node $node, $amount = 0) {

        if(!$node->user->parent_id) return;

        return Wallet::create([
            'source' => 'royalty',
            'description' => 'ROYALTY COMMISSION',
            'amount' => $amount,
            'type' => 'Cr',
            'currency' => "PHP",
            'status' => "COMPLETED",
            'user_id' => $node->user->parent_id,
            'node_id' => $node->id
        ]);
    }

    protected function get_points($package_id) {
        if($package_id == 1) return config('packages.SP.points');
        if($package_id == 2) return config('packages.BP.points');
        if($package_id == 3) return config('packages.AP.points');
    }

    protected function get_max_matched($package_id) {
        if($package_id == 1) return config('commissions.match.SP.match.max');
        if($package_id == 2) return config('commissions.match.BP.match.max');
        if($package_id == 3) return config('commissions.match.AP.match.max');
    }

    public function get_pair_count($node_id) {
        return Wallet::where(['node_id' => $node_id])
            ->whereDate('created_at',$this->dateToday)->count();
    }

    public function isPairLessThenMax($node) {
        $pair_max = $this->get_max_matched($node->package_id);
        $pair_count = $this->get_pair_count($node->id);
        if($pair_count < $pair_max) {
            return true;
        }
        return false;
    }

    public function isDateToday($date) {
        if(Carbon::parse($date) == $this->dateToday) return true;
        return false;
    }


}
