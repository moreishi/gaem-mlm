<?php

/**
 * After the node is activated or upgraded. This event listener is triggered.
 * The purpose of this class is to add credit to the leadership fund pool.
 */


namespace App\Listeners\AccountUpgrade;

use App\Events\AccountUpgradeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

use App\Wallet;

class AddLeadershipFundListener
{

    public $user;

    public $node;

    public $package;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  AccountUpgradeEvent  $event
     * @return void
     */
    public function handle(AccountUpgradeEvent $event)
    {
        $this->user = $event->user;

        $this->node = $event->node;

        $this->package = $event->package;

        $this->add_leadership_fund();

    }

    public function add_leadership_fund()
    {
        $amount = $this->get_leadership_value($this->node->package_id);

        $this->dispatch_leadership_fund($amount);
    }

    public function dispatch_leadership_fund($amount)
    {

        Wallet::create([
            'type' => 'Cr',
            'description' => 'WEEKLY LEADERSHIP POINTS COMMISSION',
            'amount' => $amount,
            'status' => 'COMPLETED',
            'currency' => 'PHP',
            'source' => 'leadership'
        ]);

    }

    public function get_leadership_value($package_id) {

        if($package_id == 1) return 18;     // SP

        if($package_id == 2) return 28;     // BP

        if($package_id == 3) return 38;     // AP

        return 0;

    }

}
