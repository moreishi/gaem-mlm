<?php

namespace App\Listeners\AccountUpgrade;

use App\Events\AccountUpgradeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AccountUpgradeListener
{

    public $user;

    public $node;

    public $package;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        Log::info("ACCOUNT_UPGRADE_LISTENER_TRIGGERED",[]);
    }

    /**
     * Handle the event.
     *
     * @param  AccountUpgradeEvent  $event
     * @return void
     */
    public function handle(AccountUpgradeEvent $event)
    {
        $this->user = $event->user;

        $this->node = $event->node;

        $this->package = $event->package;

        $this->upgrade();

    }

    public function upgrade() {

        $this->node->package_id = $this->package->id;
        $this->node->save();

    }

}
