<?php

/**
 * UPDATE THE TREE COUNT LEFT LEG AND RIGHT LEG.
 */

namespace App\Listeners\AccountUpgrade;

use App\Events\AccountUpgradeEvent;
use App\Node;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AccountUpdateDescendantsListener
{

    public $user;

    public $node;

    public $package;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        Log::info("ACCOUNT_UPDATE_DESCENDANT_LISTENER_TRIGGERED",[]);
    }

    /**
     * Handle the event.
     *
     * @param  AccountUpgradeEvent  $event
     * @return void
     */
    public function handle(AccountUpgradeEvent $event)
    {
        $this->user = $event->user;

        $this->node = $event->node;

        $this->package = $event->package;

        $this->update_ancestors_count($this->node);

    }


    public function update_ancestors_count(Node $node) {

        if(!$node->parent_id) return;

        $parent_node = $this->get_parent_node($node->parent_id);
        Log::info("PARENT",[$parent_node]);
        if($node->id == $parent_node->lft) {
            $parent_node->lcount++;
            $parent_node->save();
        } elseif ($node->id == $parent_node->rgt) {
            $parent_node->rcount++;
            $parent_node->save();
        }

        $this->update_ancestors_count($parent_node);

    }

    public function get_parent_node($parent_id) {
        return Node::find($parent_id);
    }

}
