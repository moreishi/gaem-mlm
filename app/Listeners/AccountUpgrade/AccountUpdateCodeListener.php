<?php

namespace App\Listeners\AccountUpgrade;

use App\Events\AccountUpgradeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

use App\Code;

class AccountUpdateCodeListener
{

    public $user;

    public $node;

    public $package;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        Log::info("ACCOUNT_UPDATE_CODE_LISTENER_TRIGGERED",[]);
    }

    /**
     * Handle the event.
     *
     * @param  AccountUpgradeEvent  $event
     * @return void
     */
    public function handle(AccountUpgradeEvent $event)
    {
        $this->user = $event->user;

        $this->node = $event->node;

        $this->package = $event->package;

        $this->update_code();

    }

    public function update_code() {

        $code = Code::where('user_id',$this->node->user_id)
            ->where('status','VALID')
            ->where('package_id',$this->package->id)
            ->first();

        $code->status = "USED";
        $code->save();

    }

}
