<?php

namespace App\Listeners\AccountUpgrade;

use App\Events\AccountUpgradeEvent;
use App\Events\ReferralCommission;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * On successful account upgrade. This class will dispatch the bonus for the direct upline.
 * @package App\Listeners\AccountUpgrade
 */

class DispatchUplineBonus
{

    public $user;

    public $node;

    public $package;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AccountUpgradeEvent  $event
     * @return void
     */
    public function handle(AccountUpgradeEvent $event)
    {
        $this->user = $event->user;

        $this->node = $event->node;

        $this->package = $event->package;

        event(new ReferralCommission(User::find($this->node->user_id),$this->node));

    }



}
