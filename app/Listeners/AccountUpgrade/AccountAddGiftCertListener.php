<?php

/**
 * Gift Certificates will be triggered whenever new node is created or upgraded.
 * This will add a new Gift Certificate to the user account owner.
 */

namespace App\Listeners\AccountUpgrade;

use App\Events\AccountUpgradeEvent;
use App\Gift;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AccountAddGiftCertListener
{

    public $user;

    public $node;

    public $package;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  AccountUpgradeEvent  $event
     * @return void
     */
    public function handle(AccountUpgradeEvent $event)
    {

        $this->user = $event->user;

        $this->node = $event->node;

        $this->package = $event->package;

        $this->dispatch_gift_certificate($this->package->code);
    }

    /**
     * @param $packageCode
     */
    public function dispatch_gift_certificate($packageCode) {

        switch ($packageCode) {

            case 'SP':
                $gc_value = config('packages.SP.addOn.gc.value');
                $gc_quantity = config('packages.SP.addOn.gc.quantity');
                $this->distribute_gift_certificate($this->node->user_id, $gc_quantity, $gc_value);
                break;
            case 'BP':
                $gc_value = config('packages.BP.addOn.gc.value');
                $gc_quantity = config('packages.BP.addOn.gc.quantity');
                $this->distribute_gift_certificate($this->node->user_id, $gc_quantity, $gc_value);
                break;
            case 'AP':
                $gc_value = config('packages.AP.addOn.gc.value');
                $gc_quantity = config('packages.AP.addOn.gc.quantity');
                $this->distribute_gift_certificate($this->node->user_id, $gc_quantity, $gc_value);
                break;

        }

    }

    /**
     * @param $user_id
     * @param $quantity
     * @param $value
     */
    private function distribute_gift_certificate($user_id, $quantity, $value)
    {
        for($i = 0; $i < $quantity; $i++) {
            Gift::create([
                'user_id' => $user_id,
                'amount' => $value,
                'status' => 'VALID',
                'code' => rand(1000,9999).time()

            ]);
        }
    }

}
