<?php

namespace App\Listeners\TravelFund;

use App\Events\TravelFund;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;
use App\Wallet;
use App\Node;
use App\User;

class AddTravelFund
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TravelFund  $event
     * @return void
     */
    public function handle(TravelFund $event)
    {
        $amount = $this->getTravelFundValue($event->node->package_id);
        if($this->isPostLessThenMaxPost($event)) {
            $this->addTravelFund($event,$amount);
        }
    }

    public function addTravelFund($event, $amount = 0) {
        return Wallet::create([
            'source' => 'travel',
            'description' => 'TRAVEL FUND',
            'type' => 'Cr',
            'currency' => "PHP",
            'status' => "COMPLETED",
            'amount' => $amount,
            'user_id' => $event->user->id,
            'node_id' => $event->node->id
        ]);
    }

    public function getTravelFundValue($package_id) {
        $amount = 0;
        if($package_id == 1) $amount = config('commissions.travel.SP.amount');
        if($package_id == 2) $amount = config('commissions.travel.BP.amount');
        return $amount;
    }

    public function get_max_post_package($package_id) {
        $post= 0;
        if($package_id == 1) $post = config('commissions.travel.SP.max_post');
        if($package_id == 2) $post = config('commissions.travel.BP.max_post');
        return (int) $post;
    }

    public function get_travel_post_count($event) {
        return Wallet::where(['node_id' => $event->node->id,
            'user_id' => $event->user->id,'source' => 'travel'])
                    ->whereDate('created_at',Carbon::today())->count();
    }

    public function isPostLessThenMaxPost($event) {
        $max_post = $this->get_max_post_package($event->node->package_id);
        $count_post = $this->get_travel_post_count($event);
        if($count_post < $max_post) {
            return true;
        }
        return false;
    }

}
