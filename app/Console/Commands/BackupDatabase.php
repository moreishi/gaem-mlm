<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $db_user = config('database.connections.mysql.username');
        $db_pw = config('database.connections.mysql.password');
        $database = config('database.connections.mysql.database');

        $this->process = new Process(sprintf(
            'mysqldump -u%s -p%s %s > %s',
            $db_user,
            $db_pw,
            $database,
            storage_path('backups/'.$database.'_'.\Carbon\Carbon::now()->timestamp.'.sql')
        ));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->process->mustRun();

            $this->info('The backup has been proceed successfully.');
        } catch (ProcessFailedException $exception) {
            $this->error('The backup process has been failed.');
        }
    }
}
