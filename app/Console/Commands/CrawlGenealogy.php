<?php

namespace App\Console\Commands;

use App\Events\Jobs\CrawlGenealogyEvent;
use Illuminate\Console\Command;
use App\Node;
use Log;

class CrawlGenealogy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'genealogy:crawl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will crawl the nodes and update the searchable tree of the user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach(Node::latest()->get() as $node)
        {
            event(new CrawlGenealogyEvent($node));
        }
    }
}
