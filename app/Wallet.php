<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use Carbon\Carbon;

class Wallet extends Model
{

//    public $fillable = [
//        'source',
//        'description',
//        'amount',
//        'type',
//        'wallet_type',
//        'currency',
//        'status',
//        'data',
//        'user_id',
//        'node_id'
//    ];

    protected $guarded = [];

    public function user() {
        return $this->belongsTo(\App\User::class);
    }

    public function credit() {
        return $this->where('type','Cr')->sum('amount');
    }

    public function debit() {
        return $this->where('type','Dr')->sum('amount');
    }

    public function withdrawable() {
        return $this->where(['type' => 'Cr','source' => 'cashout'])->sum('amount');
    }

    public function sum_balance_pairing() {
        return $this->where('source','pairing')->sum('balance');
    }

    public function nodes(){
        return $this->belongsTo(App\Node::class);
    }

    public function node_balance() {
        return $this->where('type','Cr')->sum('amount');
    }

    public function referral() {
        return $this->orWhere('description','LIKE','%REFERRAL%')->sum('amount');
    }

    public function pairing() {
        return $this->orWhere('description','LIKE','%MATCH COMMISSION%')->sum('amount');
    }

    public function matchWeek() {
        return $this->where(['source' => 'pairing']);
    }

    public function walletSum() {
        return $this->sum('amount');
    }

    public static function global_pool() {

        $today = Carbon::today();

        $month_start = $today->startOfMonth()->toDateTimeString();
        $month_end = $today->endOfMonth()->toDateTimeString();

        return Wallet::where('description','LIKE','%GLOBAL POOL COMMISSION%')
            ->whereBetween('created_at',[$month_start,$month_end])
            ->sum('amount');

    }

}
