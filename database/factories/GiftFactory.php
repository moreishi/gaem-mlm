<?php

use Faker\Generator as Faker;

$factory->define(App\Gift::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->randomNumber(4).time(),
        'amount' => 300.00,
        'status' => 'VALID',
        'user_id' => 1,
    ];
});
