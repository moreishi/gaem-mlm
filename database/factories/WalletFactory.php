<?php

use Faker\Generator as Faker;

$factory->define(App\Wallet::class, function (Faker $faker) {
    return [
        'source' => 'direct-invite',
        'description' => 'REFERRAL COMMISSION',
        'amount' => 100.00,
        'type' => 'Cr',
        'currency' => "PHP",
        'status' => "COMPLETED",
        'user_id' => 1
    ];
});
