<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Scalar',
            'sku' => 'scalar',
            'long_description' => 'Sample long scalar',
            'short_description'=> 'Sample short description',
            'regular_price'=> 120.00,
            'sale_price' => 120.00,
            'currency' => 'PHP',
            'pv' => 40,
            'stock_status' => 'in-stock',
        ]);

        Product::create([
            'name' => 'Mega Rub',
            'sku' => 'mega_rub',
            'long_description' => 'Sample long Mega Rub',
            'short_description'=> 'Sample short Mega Rub',
            'regular_price'=> 90.00,
            'sale_price' => 90.00,
            'currency' => 'PHP',
            'pv' => 20,
            'stock_status' => 'in-stock',
        ]);

        Product::create([
            'name' => 'Coffee',
            'sku' => 'coffee',
            'long_description' => 'Sample long Coffee',
            'short_description'=> 'Sample short Coffee',
            'regular_price'=> 144.00,
            'sale_price' => 144.00,
            'currency' => 'PHP',
            'pv' => 40,
            'stock_status' => 'in-stock',
        ]);

        Product::create([
            'name' => 'Vaporeen',
            'sku' => 'vaporeen',
            'long_description' => 'Sample long Vaporeen',
            'short_description'=> 'Sample short Vaporeen',
            'regular_price'=> 90.00,
            'sale_price' => 90.00,
            'currency' => 'PHP',
            'pv' => 30,
            'stock_status' => 'in-stock',
        ]);

    }
}
