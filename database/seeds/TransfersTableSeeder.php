<?php

use Illuminate\Database\Seeder;

class TransfersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            \App\Transfer::create([
                'desc' => 'You transfered code to Ian',
                'package_id' => 1,
                'user_id' => 1,
            ]);
        }

        for ($i = 0; $i < 10; $i++) {
            \App\Transfer::create([
                'desc' => 'You transfered code to Super2',
                'package_id' => 2,
                'user_id' => 1,
            ]);
        }

        for ($i = 0; $i < 10; $i++) {
            \App\Transfer::create([
                'desc' => 'You transfered code to Super3',
                'package_id' => 3,
                'user_id' => 1,
            ]);
        }
    }
}
