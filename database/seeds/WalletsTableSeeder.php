<?php

use Illuminate\Database\Seeder;
use App\Wallet;

class WalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 10; $i++) {

            $source = ['pairing','unilevel','direct-invite','bonus','product'];
            $money = [100.00, 250.00, 300.00, 150.00];
            $type = ['Dr','Cr'];
            $currency = ['USD','PHP','EUR'];

            $amount_pick = $money[rand(0,count($money)-1)];
            $type_pick = $type[rand(0,count($type)-1)];
            $source_pick = ($type_pick == "Dr") ? 'withdrawal' : $source[rand(0,count($source)-1)];
            $currency_pick = $currency[rand(0,count($currency)-1)];

            Wallet::create([
                'source' => $source_pick,
                'description' => $source_pick,
                'amount' => $amount_pick,
                'type' => $type_pick,
                'currency' => "PHP",
                'status' => "COMPLETED",
                'user_id' => 1
            ]);
        }

    }
}
