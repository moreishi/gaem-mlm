<?php

use Illuminate\Database\Seeder;

use App\Order;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $currency = config('currency.default');

        $scalar = json_encode(["scalar" => 1,"mega_rub" => 0,"coffee" => 0,"vaporeen" => 0]);

        factory(Order::class,100)->create([
            'user_id' => 5,
            'data' => $scalar,
            'status' => 'VALID',
            'subject' => 'SCALAR ENERGY SAVER STICKER @ 1pc'
        ]);

        $coffee = json_encode(["scalar" => 0,"mega_rub" => 0,"coffee" => 1,"vaporeen" => 0]);

        factory(Order::class,100)->create([
            'user_id' => 5,
            'data' => $coffee,
            'status' => 'VALID',
            'subject' => 'MEGAMIX COFFEE @ 1pc'
        ]);

        factory(Order::class,100)->create([
            'user_id' => 4,
            'data' => $scalar,
            'status' => 'VALID',
            'subject' => 'SCALAR ENERGY SAVER STICKER @ 1pc'
        ]);

        $coffee = json_encode(["scalar" => 0,"mega_rub" => 0,"coffee" => 1,"vaporeen" => 0]);

        factory(Order::class,100)->create([
            'user_id' => 4,
            'data' => $coffee,
            'status' => 'VALID',
            'subject' => 'MEGAMIX COFFEE @ 1pc'
        ]);

    }
}
