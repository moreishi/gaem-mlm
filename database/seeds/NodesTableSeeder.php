<?php

use Illuminate\Database\Seeder;
use App\Node;
use Carbon\Carbon;
class NodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $node = Node::create([
            'name' => 'SuperAdmin',
            'package_id' => 2,
            'points' => 3,
            'user_id' => 1,
            'data' => json_encode([
                'match' => ['count' => 30, 'updated_at' => Carbon::today()]
            ])
        ]);

        $node = Node::create([
            'name' => 'Acct1a',
            'package_id' => 1,
            'points' => 2,
            'user_id' => 2,
            'parent_id' => 1,
            'data' => json_encode([
                'match' => ['count' => 10, 'updated_at' => Carbon::today()]
            ])
        ]);

        $node = Node::create([
            'name' => 'Acct1b',
            'package_id' => 1,
            'points' => 2,
            'user_id' => 3,
            'parent_id' => 1,
            'data' => json_encode([
                'match' => ['count' => 10, 'updated_at' => Carbon::today()]
            ])
        ]);


        $node = Node::create([
            'name' => 'Acct1c',
            'package_id' => 1,
            'points' => 2,
            'user_id' => 4,
            'parent_id' => 1,
            'data' => json_encode([
                'match' => ['count' => 10, 'updated_at' => Carbon::today()]
            ])
        ]);



    }

    private function generate_node($limit = 0) {

            $node = Node::create([
                'name' => 'node_'.rand(10,100),
                'package_id' => 1,
                'user_id' => 0,
            ]);
            $node->makeRoot();

            if($limit > 0) {
                $this->generate_node($limit--);
            }

    }
}
